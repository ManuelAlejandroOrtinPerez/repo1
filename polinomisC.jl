#module PolinomisC1
using Polynomial
export arrels
#arrels = p -> filter(isreal, roots(p))
function arrels(p)
        res=[Int64]
        if (length(p)==1)
                return res
        elseif (length(p)==2)
                     a=p[1]
                     b=p[2]
                     c=((a*(-1))/b)
                     return res[c]
        elseif (length(p)==3)
                a=p[1]
                b=p[2]
                c=p[3]

                x1=(-b+sqrt((b)^2-(4*a*c)))/(2*a)
                x2=(-b-sqrt((b)^2-(4*a*c)))/(2*a)

                return res[x1,x2]
        elseif (length(p)==4)
                a=p[1]
                b=p[2]
                c=p[3]
                d=p[4]

                x1 = -b/(3*a) - (2^(1/3)*(-b^2 + 3*a*c))/(3*a*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) + (-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(3*2^(1/3)*a)
                x2 = -b/(3*a) + ((1 + i*Sqrt[3])*(-b^2 + 3*a*c))/(3*2^(2/3)*a*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) - (1 - i*Sqrt[3])*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(6*2^(1/3)*a)

                x3 = -b/(3*a) + ((1 - i*Sqrt[3])*(-b^2 + 3*a*c))/(3*2^(2/3)*a*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) - (1 + i*Sqrt[3])*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(6*2^(1/3)*a)

                return res[x1,x2,x3]
         else
                return res

end
end
