module PolinomisC2
using Polynomial
export arrels
#arrels = p -> filter(isreal, roots(p))
function arrels(p)
        res=Float64[]
        if (length(p)==1)
                return res
        end

        if (length(p)==2)
                     a=p[1]
                     b=p[2]
                     c=((b*(-1))/a)
                     res = [c]
                     return res
        end

        if (length(p)==3)
                a=p[1]
                b=p[2]
                c=p[3]
                d=(b)^2-(4*a*c)
                if d<0
                        return res
                end


                x1=(-b+sqrt((b)^2-(4*a*c)))/(2*a)
                x2=(-b-sqrt((b)^2-(4*a*c)))/(2*a)


                res = [x1,x2]

                return res
        end

        if (length(p)==4)
                a=p[1]
                b=p[2]
                c=p[3]
                d=p[4]

                x1 = -b/(3*a) - (2^(1/3)*(-b^2 + 3*a*c))/(3*a*(-re2*b^3 + 9*a*b*c - 27*a^2*d + sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) + (-2*b^3 + 9*a*b*c - 27*a^2*d + sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(3*2^(1/3)*a)
                x2 = -b/(3*a) + ((1 + im*sqrt[3])*(-b^2 + 3*a*c))/(3*2^(2/3)*a*(-2*b^3 + 9*a*b*c - 27*a^2*d + sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) - (1 - i*Sqrt[3])*(-2*b^3 + 9*a*b*c - 27*a^2*d + sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(6*2^(1/3)*a)

                x3 = -b/(3*a) + ((1 - im*sqrt[3])*(-b^2 + 3*a*c))/(3*2^(2/3)*a*(-2*b^3 + 9*a*b*c - 27*a^2*d + sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) - (1 + i*Sqrt[3])*(-2*b^3 + 9*a*b*c - 27*a^2*d + sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(6*2^(1/3)*a)

                res = [real(x1),real(x2),real(x3)]

                return res
         end

         return res
end
end
